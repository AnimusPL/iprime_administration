class Samba_Worker:
    def __init__(self):
        self._menu_option = str(input("DPERSON/SAMBA - KONFIGURACJA\n"
                                      "--------------------------------------------------\n"
                                      "Wybierz opcję:\n"
                                      "--------------------------------------------------\n"
                                      "1. Skopiuj plik docker-compose.yml z serwera do folderu IT\n"
                                      "2. Skopiuj zmodyfikowany plik docker-compose.yml z folderu IT do serwera\n"
                                      "3. Zainicjuj nowe ustawienia i zrestartuj kontener Samby\n"
                                      "4. Wyjdź do menu głównego\n"
                                      "(Wprowadź odpowiedni numer i zatwierdź za pomocą klawisza ENTER.)\n"))
        self._docker_compose_path_command = "cd /home/docker"
        self._docker_compose_copy_command = "cp docker-compose.yml /home/docker/samba/groups/it"
        self._docker_compose_copy_back_command = "cp /home/docker/samba/groups/it/docker-compose.yml ."
        self._docker_compose_init_command = "docker-compose up -b"

    def get_menu_option(self):
        return self._menu_option

    def move_to_docker_compose_yml_localisation(self):
        return self._docker_compose_path_command

    def copy_docker_compose_yml(self):
        return self._docker_compose_copy_command

    def copy_back_docker_compose_yml(self):
        return self._docker_compose_copy_back_command

    def init_docker_compose_yml(self):
        return self._docker_compose_init_command
