import paramiko


class Connector:
    def __init__(self):
        self._server_ip = "220.20.0.5"
        self._username = ""
        self._password = ""
        self._ssh = ""
        self._ssh_stdin, self._ssh_stdout, self._ssh_stderr = "", "", ""
        self._connection_error = "Błąd w połączeniu! Prawdopobnie podano niewłaściwą nazwę użytkownika bądź hasło."
        self._connected = False

    def set_username(self):
        self._username = str(input("Podaj nazwę użytkownika: "))

    def set_password(self):
        self._password = input("Podaj hasło:")

    def set_connection(self):
        self._ssh = paramiko.SSHClient()
        try:
            self._ssh.connect(self._server_ip, username=self._username, password=self._password)
            self._connected = True
        except TimeoutError:
            print(self._connection_error)

    def execute_command(self, command):
        self._ssh_stdin, self._ssh_stdout, self._ssh_stderr = self._ssh.exec_command(command)

    def get_connection_status(self):
        return self._connected
