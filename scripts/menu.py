class Menu:
    def __init__(self):
        self._menu_option = ""

    def starting(self):
        print(''' _______     _
    (_) ___ \   (_)
     _| |_/ / __ _ _ __ ___   ___
    | |  __/ '__| | '_ ` _ \ / _ \\
    | | |  | |  | | | | | | |  __/
    |_\_|  |_|  |_|_| |_| |_|\___|''', 'Administration v. 0.1 (17.09.2020)\n')

    def set_menu_option(self):
        self._menu_option = str(input("MENU GŁÓWNE\n"
                                     "--------------------------------------------------\n"
                                     "Wybierz opcję:\n"
                                     "--------------------------------------------------\n"
                                     "1. Konfiguracja użytkowników i zasobów SAMBY\n"
                                     "2. Tworzenie dodatkowego klienta OpenVPN\n"
                                     "3. Wyjdź z programu\n"
                                     "(Wprowadź odpowiedni numer i zatwierdź za pomocą klawisza ENTER.)\n"))

    def get_menu_option(self):
        return self._menu_option
