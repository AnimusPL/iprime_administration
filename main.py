from scripts.connector import Connector
from scripts.menu import Menu
from scripts.samba_worker import Samba_Worker
from scripts.vpn_worker import VPN_Worker

menu = Menu()
ssh_connector = Connector()

if __name__ == '__main__':
    ssh_connector.set_username()
    ssh_connector.set_password()
    ssh_connector.set_connection()
    while ssh_connector.get_connection_status():
        menu.starting()
        menu.set_menu_option()
        if menu.get_menu_option() == '1':
            samba_worker = Samba_Worker()
            ssh_connector.execute_command(samba_worker.move_to_docker_compose_yml_localisation())
            if samba_worker.get_menu_option() == "1":
                ssh_connector.execute_command(samba_worker.copy_docker_compose_yml())
            elif samba_worker.get_menu_option() == "2":
                ssh_connector.execute_command(samba_worker.copy_back_docker_compose_yml())
            elif samba_worker.get_menu_option() == "3":
                ssh_connector.execute_command(samba_worker.init_docker_compose_yml())
            elif samba_worker.get_menu_option() == "4":
                pass
        elif menu.get_menu_option() == '2':
            vpn_worker = VPN_Worker()
        elif menu.get_menu_option() == '3':
            exit()